package br.com.glaucomachado.bookfinder;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import java.util.List;

/**
 * Created by Glauco Machado on 05/08/18.
 */
public class BookLoader extends AsyncTaskLoader<List<Book>>{

    /**
     * Tag for log messages
     */
    private static final String LOG_TAG = BookLoader.class.getName();

    private String[] urls;

    BookLoader(Context context, String... urls) {
        super(context);
        this.urls = urls;
    }

    @Override
    protected void onStartLoading() {
        Log.i("Deb: ", "Entrei no onStartLoading");
        forceLoad();
        Log.i("Deb: ", "Depois do forceLoading no onStartLoading");
    }

    @Override
    public List<Book> loadInBackground() {
        Log.i("Deb: ", "Entrei no loadInBackground");
        // Don't perform the request if there are no URLs, or the first URL is null.
        if (urls.length < 1 || urls[0] == null) {
            return null;
        }

        // Perform the HTTP request for earthquake data and process the response.
        Log.i("Deb: ", "Vou chamar fetchBookData no loadInBackground");
        return Utils.fetchBookData(urls[0]);
    }
}
