package br.com.glaucomachado.bookfinder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Glauco Machado on 04/08/18.
 */
public class BookAdapter extends ArrayAdapter<Book> {

    public BookAdapter(@NonNull Context context, @NonNull List<Book> books) {
        super(context, 0, books);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listItemView = convertView;
        if (listItemView == null){
            listItemView = LayoutInflater.from(getContext())
                    .inflate(R.layout.book_details, parent, false);
        }

        Book currentBook = getItem(position);

        if (currentBook != null){
            TextView bookTitleView = listItemView.findViewById(R.id.book_title);
            bookTitleView.setText(currentBook.getTitle());

            TextView bookAuthorView = listItemView.findViewById(R.id.book_author);
            bookAuthorView.setText(currentBook.getAuthor());
        }
        return listItemView;
    }
}
